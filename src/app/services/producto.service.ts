import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs/Observable';
import {Producto} from '../models/producto';
import {GLOBAL} from './global';

@Injectable()
export class ProductoService{

  public url:string;

  constructor(//Para poder hacer peticiones ajax
    public _httpClient:HttpClient
  ){
    this.url = GLOBAL.url;
  }

  getProductos():Observable<any>{
    return this._httpClient.get(this.url+'productos');//Conectando con el backend
  }

  getProductoDetalle(id):Observable<any>{
    return this._httpClient.get(this.url+'producto/'+id);
  }

  addProducto(producto:Producto):Observable<any>{
    let json = JSON.stringify(producto);
    let params = 'json='+json;
    let headers = new HttpHeaders({'Content-Type':'application/x-www-form-urlencoded'});

    return this._httpClient.post(this.url+'productos', params, { headers: headers });
  }

  editProducto(id,producto:Producto):Observable<any>{
    let json = JSON.stringify(producto);
    let params = 'json='+json;
    let headers = new HttpHeaders({'Content-Type':'application/x-www-form-urlencoded'});

    return this._httpClient.post(this.url+'producto-edit/'+id, params, { headers: headers });
  }

  deleteProducto(id):Observable<any>{
    return this._httpClient.get(this.url+'producto-delete/'+id);
  }

  makeFileRequest(url: string, params: Array<string>, files: Array<File>){
    return new Promise((resolve, reject)=>{
      var formData: any = new FormData();//Simular Formulario
      var xhr = new XMLHttpRequest();//Tener disponible el objeto para peticion ajax

      for (var i = 0; i < files.length; i++){
        formData.append('uploads[]', files[i], files[i].name);
      }

      xhr.onreadystatechange = function(){//Cuando la peticiona ajax esté preparada
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          }else{
            reject(xhr.response);
          }
        }
      };

      xhr.open('POST',url,true);//Abrir
      xhr.send(formData);//Enviar el formulario hacia la url por POST
    });
  }



}

