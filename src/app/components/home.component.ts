import {Component} from '@angular/core';

@Component({
  selector: 'home',
  templateUrl: '../views/home.html'
})

export class HomeComponent{

  public titulo: string;

  constructor(){
    this.titulo = 'Webapp Angular 9';
  }

  ngOnInit(){
    console.log('Se a cargado correctamente el home.component.ts');
  }

}
