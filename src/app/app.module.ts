import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

//Añadir rutas
import {routing, appRoutingProviders} from './app.routing';
//Añadir componentes
import {HomeComponent} from './components/home.component';
import {ErrorComponent} from './components/error.component';
import {ProductoListComponent} from './components/producto-list.component';
import {ProductoAddComponent} from './components/producto-add.component';
import {ProductoDetailComponent} from './components/producto-detail.component';
import {ProductoEditComponent} from './components/producto-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ErrorComponent,
    ProductoListComponent,
    ProductoAddComponent,
    ProductoDetailComponent,
    ProductoEditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    routing
  ],
  providers: [
    appRoutingProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
