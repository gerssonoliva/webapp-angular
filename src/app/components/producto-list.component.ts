import {Component} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
//Importando el servicio
import {ProductoService} from '../services/producto.service';
//Importar modelo
import {Producto} from '../models/producto';

@Component({
  selector: 'producto-list',
  templateUrl: '../views/producto-list.html',
  //inyectar en componente
  providers: [ProductoService]
})

export class ProductoListComponent{

  public titulo:string;
  public productos:Producto[];
  public confirmado;

  constructor(
    private _route:ActivatedRoute,
    private _router:Router,
    private _productoService:ProductoService
  ){
    this.titulo = 'Listado de Productos';
    this.confirmado = null;
  }

  ngOnInit(){
    console.log('producto-list.component.ts cargado');
    this.getProductos();

  }

  getProductos(){
    this._productoService.getProductos().subscribe(
      result => {
        if (result.code != 200) {
          console.log(result);
        }else{
          this.productos = result.data;
        }
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  deleteConfirm(id){
    this.confirmado = id;
  }

  cancelConfirm(){
    this.confirmado = null;
  }

  onDeleteProducto(id){
    this._productoService.deleteProducto(id).subscribe(
      response => {
        if (response.code == 200) {
          this.getProductos();
        }else{
          alert('Error al eliminar el producto');
        }
      },
      error => {
        console.log(<any>error);
      }
    );
  }
}
