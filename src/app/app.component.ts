import { Component } from '@angular/core';
//Importando el servicios Global
import {GLOBAL} from './services/global';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'CursoAngular-Udemy-Webapp';
  public header_color:string;

  constructor(){
    this.header_color = GLOBAL.header_color;
  }
}
